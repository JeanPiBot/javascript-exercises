const array = [10,30,2, 8, 90, 28];

const productos = [
    { id: 1, nombre:"t shirt", precio: 20 },
    { id: 2, nombre: "Skirt", precio: 50 },
    { id: 3, nombre: "shoes", precio: 40 },
    { id: 4, nombre: "Jacket", precio: 25 }
]
// Map sirve para crear un nuevo array


const productosConDescuentos = productos.map((producto) => {
    if (producto.precio < 30) return producto;

    return {
        ...producto,
        precio: producto.precio * 0.9
    }
})

console.log(productos)
console.log(productosConDescuentos)